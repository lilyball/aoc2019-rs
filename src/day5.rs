use crate::intcode::*;
use anyhow::{Context, Result};
use aoc_runner_derive::{aoc, aoc_generator};
use std::num::ParseIntError;

#[aoc_generator(day5)]
fn input_generator(input: &str) -> Result<Vec<i32>, ParseIntError> {
    input.split(",").map(|s| s.parse()).collect()
}

#[aoc(day5, part1)]
fn part1(input: &[i32]) -> Result<i32> {
    let mut prog = Intcode::from(input);
    let output = prog.run_program(vec![1])?;
    output.last().map(|&x| x).context("no diagnostic code")
}

#[aoc(day5, part2)]
fn part2(input: &[i32]) -> Result<i32> {
    let mut prog = Intcode::from(input);
    let output = prog.run_program(vec![5])?;
    output.last().map(|&x| x).context("no diagnostic code")
}
