#![allow(unused)]

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct Size {
    pub width: i32,
    pub height: i32,
}

impl Size {
    pub const fn new(width: i32, height: i32) -> Size {
        Size { width, height }
    }

    /// Returns a `Size` where both dimensions are positive.
    pub fn normalized(&self) -> Size {
        Size {
            width: self.width.abs(),
            height: self.height.abs(),
        }
    }
}

impl std::fmt::Display for Size {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{{{}, {}}}", self.width, self.height)
    }
}

impl From<(i32, i32)> for Size {
    fn from(val: (i32, i32)) -> Size {
        Size {
            width: val.0,
            height: val.1,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct Coord {
    pub x: i32,
    pub y: i32,
}

impl Coord {
    pub const fn new(x: i32, y: i32) -> Coord {
        Coord { x, y }
    }

    pub const fn manhattan_distance_to(&self, other: Coord) -> u32 {
        (self.x - other.x).abs() as u32 + (self.y - other.y).abs() as u32
    }
}

impl From<(i32, i32)> for Coord {
    fn from(val: (i32, i32)) -> Coord {
        Coord { x: val.0, y: val.1 }
    }
}

impl std::fmt::Display for Coord {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{{{}, {}}}", self.x, self.y)
    }
}

#[derive(PartialEq, Eq, Hash)]
pub struct Grid<T> {
    items: Vec<T>,
    size: Size,
}

impl<T> Grid<T>
where
    T: Clone,
{
    pub fn new_clone(size: Size, value: &T) -> Grid<T> {
        Grid::new_with(size, |_| value.clone())
    }
}

impl<T> Grid<T>
where
    T: Default,
{
    pub fn new_default(size: Size) -> Grid<T> {
        Grid::new_with(size, |_| <T as Default>::default())
    }
}

impl<T> Grid<T> {
    fn new_unfilled(size: Size) -> Grid<T> {
        let size = size.normalized();
        Grid {
            items: Vec::with_capacity((size.width * size.height) as usize),
            size,
        }
    }

    /// Creates a new grid by calling `f` once for each cell.
    /// The function is called in row-major order.
    pub fn new_with<F>(size: Size, mut f: F) -> Grid<T>
    where
        F: FnMut(Coord) -> T,
    {
        let mut grid = Grid::new_unfilled(size);
        for y in 0..size.height {
            for x in 0..size.width {
                grid.items.push(f(Coord::new(x, y)));
            }
        }
        assert!(grid.items.len() == (size.width * size.height) as usize);
        grid
    }

    /// Creates a new empty grid.
    ///
    /// This is primarily intended for using with `clone_from()` later.
    pub fn new_empty() -> Grid<T> {
        Grid {
            items: Vec::new(),
            size: Size::new(0, 0),
        }
    }

    pub fn size(&self) -> Size {
        self.size
    }

    pub fn width(&self) -> i32 {
        self.size.width
    }

    pub fn height(&self) -> i32 {
        self.size.height
    }

    pub fn get(&self, at: Coord) -> Option<&T> {
        coord_to_index(at, self.size).map(|i| &self.items[i])
    }

    pub fn get_mut(&mut self, at: Coord) -> Option<&mut T> {
        coord_to_index(at, self.size).map(move |i| &mut self.items[i])
    }

    pub unsafe fn get_unchecked(&self, at: Coord) -> &T {
        self.items
            .get_unchecked(coord_to_index_unchecked(at, self.size))
    }

    pub unsafe fn get_unchecked_mut(&mut self, at: Coord) -> &mut T {
        self.items
            .get_unchecked_mut(coord_to_index_unchecked(at, self.size))
    }

    /// Swaps two elements in the grid.
    ///
    /// Panics if `a` or `b` are out of bounds.
    pub fn swap(&mut self, a: Coord, b: Coord) {
        self.items.swap(
            coord_to_index(a, self.size).expect("coord out of bounds for grid"),
            coord_to_index(b, self.size).expect("coord out of bounds for grid"),
        );
    }

    /// Iterates the grid in row-major order, yielding each coordinate.
    pub fn iter_coords(&self) -> impl Iterator<Item = Coord> {
        let size = self.size;
        (0..self.items.len()).map(move |i| index_to_coord_unchecked(i, size))
    }

    /// Iterates the grid in row-major order, yielding each coordinate along with its item.
    pub fn iter<'a>(&'a self) -> impl Iterator<Item = (Coord, &'a T)> {
        self.items
            .iter()
            .enumerate()
            .map(move |(i, elt)| (index_to_coord_unchecked(i, self.size), elt))
    }

    /// Iterates the grid in row-major order, yielding each coordinate along with its item.
    pub fn iter_mut<'a>(&'a mut self) -> impl Iterator<Item = (Coord, &'a mut T)> {
        let size = self.size;
        self.items
            .iter_mut()
            .enumerate()
            .map(move |(i, elt)| (index_to_coord_unchecked(i, size), elt))
    }
}

impl<T: Clone> Clone for Grid<T> {
    fn clone(&self) -> Self {
        Grid {
            items: self.items.clone(),
            size: self.size,
        }
    }

    fn clone_from(&mut self, source: &Self) {
        self.items.clone_from(&source.items);
        self.size = source.size;
    }
}

fn coord_to_index(coord: Coord, size: Size) -> Option<usize> {
    debug_assert!(size.width >= 0 && size.height >= 0);
    if coord.x < 0 || coord.x >= size.width || coord.y < 0 || coord.y >= size.height {
        None
    } else {
        Some(coord.y as usize * size.width as usize + coord.x as usize)
    }
}

fn coord_to_index_unchecked(coord: Coord, size: Size) -> usize {
    coord.y as usize * size.width as usize + coord.x as usize
}

fn index_to_coord_unchecked(index: usize, size: Size) -> Coord {
    Coord {
        x: (index % size.width as usize) as i32,
        y: (index / size.width as usize) as i32,
    }
}

impl<T> std::ops::Index<Coord> for Grid<T> {
    type Output = T;
    fn index(&self, index: Coord) -> &T {
        match self.get(index) {
            Some(x) => x,
            None => panic!(
                "index out of bounds; the coord is {} but the grid size is {}",
                index, self.size
            ),
        }
    }
}

impl<T> std::ops::IndexMut<Coord> for Grid<T> {
    fn index_mut(&mut self, index: Coord) -> &mut T {
        let size = self.size;
        match self.get_mut(index) {
            Some(x) => return x,
            None => panic!(
                "index out of bounds; the coord is {} but the grid size is {}",
                index, size
            ),
        }
    }
}

// MARK: Arithmetic

macro_rules! ref_impls {
    (mut $name:ident<$rhs:ident>, $fn_name:ident, $op:tt) => {
        impl<'a> std::ops::$name<&'a $rhs> for Coord {
            fn $fn_name(&mut self, rhs: &'a $rhs) {
                *self $op *rhs
            }
        }
    };
    (mut $name:ident, $fn_name:ident, $op:tt) => {
        ref_impls!(mut $name<Coord>, $fn_name, $op);
    };
    ($name:ident<$rhs:ident>, $fn_name:ident, $op:tt) => {
        impl<'a> std::ops::$name<&'a $rhs> for Coord {
            type Output = <Coord as std::ops::$name<$rhs>>::Output;
            fn $fn_name(self, rhs: &'a $rhs) -> Self::Output {
                self $op *rhs
            }
        }
        impl<'a> std::ops::$name<$rhs> for &'a Coord {
            type Output = <Coord as std::ops::$name<$rhs>>::Output;
            fn $fn_name(self, rhs: $rhs) -> Self::Output {
                *self $op rhs
            }
        }
        impl<'a, 'b> std::ops::$name<&'a $rhs> for &'b Coord {
            type Output = <Coord as std::ops::$name<$rhs>>::Output;
            fn $fn_name(self, rhs: &'a $rhs) -> Self::Output {
                *self $op *rhs
            }
        }
    };
    ($name:ident, $fn_name:ident, $op:tt) => {
        ref_impls!($name<Coord>, $fn_name, $op);
    };
}

impl std::ops::Add<Coord> for Coord {
    type Output = Coord;
    fn add(self, rhs: Coord) -> Coord {
        Coord {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

ref_impls!(Add, add, +);

impl std::ops::Sub<Coord> for Coord {
    type Output = Coord;
    fn sub(self, rhs: Coord) -> Coord {
        Coord {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

ref_impls!(Sub, sub, -);

impl std::ops::AddAssign<Coord> for Coord {
    fn add_assign(&mut self, rhs: Coord) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

ref_impls!(mut AddAssign, add_assign, +=);

impl std::ops::SubAssign<Coord> for Coord {
    fn sub_assign(&mut self, rhs: Coord) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

ref_impls!(mut SubAssign, sub_assign, -=);

impl std::ops::Mul<i32> for Coord {
    type Output = Coord;
    fn mul(self, rhs: i32) -> Coord {
        Coord {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

ref_impls!(Mul<i32>, mul, *);

impl std::ops::Div<i32> for Coord {
    type Output = Coord;
    fn div(self, rhs: i32) -> Coord {
        Coord {
            x: self.x / rhs,
            y: self.y / rhs,
        }
    }
}

ref_impls!(Div<i32>, div, /);

impl std::ops::MulAssign<i32> for Coord {
    fn mul_assign(&mut self, rhs: i32) {
        self.x *= rhs;
        self.y *= rhs;
    }
}

ref_impls!(mut MulAssign<i32>, mul_assign, *=);

impl std::ops::DivAssign<i32> for Coord {
    fn div_assign(&mut self, rhs: i32) {
        self.x /= rhs;
        self.y /= rhs;
    }
}

impl std::ops::Neg for Coord {
    type Output = Coord;
    fn neg(self) -> Coord {
        Coord {
            x: -self.x,
            y: -self.y,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn grid_clone_from() {
        let grid1 = Grid::new_clone(Size::new(10, 10), &1);
        let mut grid2 = Grid::new_clone(Size::new(10, 10), &2);
        let orig_ptr = &grid2.items[..] as *const _;
        grid2.clone_from(&grid1);
        assert_eq!(orig_ptr, &grid2.items[..] as *const _);
    }

    #[test]
    fn coord_manhattan_distance() {
        assert_eq!(Coord::new(3, 3).manhattan_distance_to(Coord::new(0, 0)), 6);
        assert_eq!(Coord::new(0, 0).manhattan_distance_to(Coord::new(3, 3)), 6);
        assert_eq!(Coord::new(1, 2).manhattan_distance_to(Coord::new(1, 2)), 0);
        assert_eq!(Coord::new(1, 2).manhattan_distance_to(Coord::new(2, 1)), 2);
    }
}
