use crate::grid::*;
use anyhow::{anyhow, Result};
use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::{HashMap, HashSet};

#[derive(Copy, Clone, Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn coord_delta(&self) -> Coord {
        match self {
            Direction::Up => Coord::new(0, -1),
            Direction::Down => Coord::new(0, 1),
            Direction::Left => Coord::new(-1, 0),
            Direction::Right => Coord::new(1, 0),
        }
    }
}

#[derive(Copy, Clone, Debug)]
struct Movement {
    dir: Direction,
    distance: u32,
}

impl std::str::FromStr for Movement {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Movement> {
        let (head, tail) = (
            s.get(0..1)
                .ok_or_else(|| anyhow!("invalid movement format"))?,
            s.get(1..)
                .ok_or_else(|| anyhow!("invalid movement format"))?,
        );
        Ok(Movement {
            dir: match head {
                "U" => Direction::Up,
                "D" => Direction::Down,
                "L" => Direction::Left,
                "R" => Direction::Right,
                _ => Err(anyhow!("invalid movement format"))?,
            },
            distance: tail.parse()?,
        })
    }
}

#[aoc_generator(day3)]
fn input_generator(input: &str) -> Result<(Vec<Movement>, Vec<Movement>)> {
    let mut iter = input.lines().map(|line| {
        line.split(",")
            .map(|s| s.parse())
            .collect::<Result<Vec<_>, _>>()
    });
    Ok((
        iter.next()
            .ok_or_else(|| anyhow!("missing first input line"))??,
        iter.next()
            .ok_or_else(|| anyhow!("missing second input line"))??,
    ))
}

#[aoc(day3, part1)]
fn part1(input: &(Vec<Movement>, Vec<Movement>)) -> u32 {
    let mut set = HashSet::new();
    let mut pos = Coord::new(0, 0);
    for &movement in input.0.iter() {
        let delta = movement.dir.coord_delta();
        for _ in 0..movement.distance {
            pos += delta;
            set.insert(pos);
        }
    }
    set.remove(&Coord::new(0, 0)); // remove the center just in case

    // `set` is now everything the first wire touches, minus the center
    let mut overlaps = HashSet::new();
    pos = Coord::new(0, 0);
    for &movement in input.1.iter() {
        let delta = movement.dir.coord_delta();
        for _ in 0..movement.distance {
            pos += delta;
            if set.contains(&pos) {
                overlaps.insert(pos);
            }
        }
    }
    overlaps
        .into_iter()
        .map(|c| c.manhattan_distance_to(Coord::new(0, 0)))
        .min()
        .expect("No overlaps")
}

#[aoc(day3, part2)]
fn part2(input: &(Vec<Movement>, Vec<Movement>)) -> u32 {
    let mut map = HashMap::new();
    let mut pos = Coord::new(0, 0);
    let mut steps = 0;
    for &movement in input.0.iter() {
        let delta = movement.dir.coord_delta();
        for _ in 0..movement.distance {
            steps += 1;
            pos += delta;
            map.entry(pos).or_insert(steps);
        }
    }
    map.remove(&Coord::new(0, 0)); // remove the center just in case

    // `map` is now everything the first wire touches, minus the center
    let mut overlaps = HashMap::new();
    pos = Coord::new(0, 0);
    steps = 0;
    for &movement in input.1.iter() {
        let delta = movement.dir.coord_delta();
        for _ in 0..movement.distance {
            steps += 1;
            pos += delta;
            if let Some(&steps_) = map.get(&pos) {
                overlaps.entry(pos).or_insert(steps + steps_);
            }
        }
    }
    overlaps.values().map(|&x| x).min().expect("No overlaps")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_sample() {
        let input = input_generator("R8,U5,L5,D3\nU7,R6,D4,L4").expect("input parse failure");
        assert_eq!(part1(&input), 6);
    }
}
