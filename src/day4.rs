use crate::scanner::*;
use anyhow::{ensure, Result};
use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::ops::RangeInclusive;

#[aoc_generator(day4)]
fn input_generator(input: &str) -> Result<RangeInclusive<u32>> {
    let mut scanner = Scanner::new(input);
    let start = scanner.scan()?;
    ensure!(scanner.scan_str("-"), "missing dash in input");
    let end = scanner.scan()?;
    Ok(start..=end)
}

#[aoc(day4, part1, BruteForce)]
fn part1(input: &RangeInclusive<u32>) -> usize {
    // We're just going to test every single number in the range for the criteria.
    // Input should already be 6-digit numbers but let's just clamp it to be sure.
    let range = *input.start().max(&100_000)..=*input.end().min(&999_999);
    range
        .filter(|num| {
            let s = num.to_string();
            // At least two adjacent digits are repeated
            s.chars().tuple_windows().any(|(a, b)| a == b)
            // No number is decreasing
                && s.chars()
                    .try_fold('0', |st, c| Some(c).filter(|&x| x >= st))
                    .is_some()
        })
        .count()
}

#[aoc(day4, part2, BruteForce)]
fn part2(input: &RangeInclusive<u32>) -> usize {
    // We're just going to test every single number in the range for the criteria.
    // Input should already be 6-digit numbers but let's just clamp it to be sure.
    let range = *input.start().max(&100_000)..=*input.end().min(&999_999);
    range
        .filter(|num| {
            let s = num.to_string();
            // At least one run of adjacent digits is of length 2
            s.chars().group_by(|&c| c).into_iter().any(|(_, group)| group.count() == 2)
            // No number is decreasing
                && s.chars()
                    .try_fold('0', |st, c| Some(c).filter(|&x| x >= st))
                    .is_some()
        })
        .count()
}
