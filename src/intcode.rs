use anyhow::{bail, Context, Result};
use std::convert::TryInto;

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Intcode(Vec<i32>);

impl Intcode {
    pub fn run_program(&mut self, input: impl IntoIterator<Item = i32>) -> Result<Vec<i32>> {
        let mut pc = 0;
        enum ParameterMode {
            Position,
            Immediate,
        }
        fn read_value(this: &mut Intcode, (arg, mode): (i32, ParameterMode)) -> Result<i32> {
            Ok(match mode {
                ParameterMode::Position => this[arg.try_into().context("negative position")?],
                ParameterMode::Immediate => arg,
            })
        }
        let mut input = input.into_iter();
        let mut output = vec![];
        loop {
            let instr = self[pc];
            pc += 1;
            let mut modes = instr / 100;
            macro_rules! read_param {
                () => {{
                    let arg = self[pc];
                    pc += 1;
                    let mode = match modes % 10 {
                        0 => ParameterMode::Position,
                        1 => ParameterMode::Immediate,
                        x => bail!("unknown parameter mode {}", x),
                    };
                    modes /= 10;
                    let _ = modes; // suppress unused assignment warning
                    (arg, mode)
                }};
            }
            match instr % 100 {
                1 => {
                    // add
                    let arg1 = read_param!();
                    let arg2 = read_param!();
                    let arg3 = read_param!();
                    self[arg3.0.try_into().context("negative position")?] =
                        read_value(self, arg1)? + read_value(self, arg2)?;
                }
                2 => {
                    // mul
                    let arg1 = read_param!();
                    let arg2 = read_param!();
                    let arg3 = read_param!();
                    self[arg3.0.try_into().context("negative position")?] =
                        read_value(self, arg1)? * read_value(self, arg2)?;
                }
                3 => {
                    // read input
                    let arg1 = read_param!();
                    self[arg1.0.try_into().context("negative position")?] =
                        input.next().context("ran out of input")?;
                }
                4 => {
                    // write output
                    let arg1 = read_param!();
                    output.push(read_value(self, arg1)?);
                }
                5 => {
                    // jump if true
                    let arg1 = read_param!();
                    let arg2 = read_param!();
                    if read_value(self, arg1)? != 0 {
                        pc = read_value(self, arg2)?
                            .try_into()
                            .context("negative position")?;
                    }
                }
                6 => {
                    // jump if false
                    let arg1 = read_param!();
                    let arg2 = read_param!();
                    if read_value(self, arg1)? == 0 {
                        pc = read_value(self, arg2)?
                            .try_into()
                            .context("negative position")?;
                    }
                }
                7 => {
                    // less than
                    let arg1 = read_param!();
                    let arg2 = read_param!();
                    let arg3 = read_param!();
                    self[arg3.0.try_into().context("negative position")?] =
                        (read_value(self, arg1)? < read_value(self, arg2)?) as i32;
                }
                8 => {
                    // equals
                    let arg1 = read_param!();
                    let arg2 = read_param!();
                    let arg3 = read_param!();
                    self[arg3.0.try_into().context("negative position")?] =
                        (read_value(self, arg1)? == read_value(self, arg2)?) as i32;
                }
                99 => {
                    // halt
                    return Ok(output);
                }
                op => {
                    bail!("unknown opcode: {}", op);
                }
            }
        }
    }
}

impl From<&[i32]> for Intcode {
    fn from(ops: &[i32]) -> Intcode {
        Intcode(ops.to_owned())
    }
}

impl std::ops::Index<u32> for Intcode {
    type Output = i32;
    fn index(&self, index: u32) -> &i32 {
        let Intcode(ref prog) = self;
        &prog[index as usize]
    }
}

impl std::ops::IndexMut<u32> for Intcode {
    fn index_mut(&mut self, index: u32) -> &mut i32 {
        let Intcode(ref mut prog) = self;
        &mut prog[index as usize]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day2_sample() -> Result<()> {
        let mut input = Intcode(vec![1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50]);
        input.run_program(vec![])?;
        assert_eq!(
            input,
            Intcode(vec![3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50])
        );
        Ok(())
    }
}
