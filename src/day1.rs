use aoc_runner_derive::{aoc, aoc_generator};
use std::num::ParseIntError;

fn fuel_for_mass(mass: u32) -> u32 {
    mass / 3 - 2
}

fn recursive_fuel_for_mass(mass: u32) -> u32 {
    std::iter::successors(Some(fuel_for_mass(mass)), |&m| {
        Some(m).filter(|&x| x >= 9).map(fuel_for_mass)
    })
    .sum()
}

#[aoc_generator(day1)]
fn input_generator(input: &str) -> Result<Vec<u32>, ParseIntError> {
    input.lines().map(|line| line.parse()).collect()
}

#[aoc(day1, part1)]
fn part1(input: &[u32]) -> u32 {
    input.iter().map(|&x| x).map(fuel_for_mass).sum()
}

#[aoc(day1, part2)]
fn part2(input: &[u32]) -> u32 {
    input.iter().map(|&x| x).map(recursive_fuel_for_mass).sum()
}
