pub use self::regex::*;
use std::ops::Range;

mod regex {
    pub trait CapturesExt {
        fn parse_unwrap<T>(&self, index: usize) -> T
        where
            T: std::str::FromStr,
            T::Err: std::fmt::Debug;
    }

    impl CapturesExt for regex::Captures<'_> {
        fn parse_unwrap<T>(&self, index: usize) -> T
        where
            T: std::str::FromStr,
            T::Err: std::fmt::Debug,
        {
            match self.get(index).unwrap().as_str().parse() {
                Ok(value) => value,
                Err(err) => panic!("regex capture group {} failed to parse: {:?}", index, err),
            }
        }
    }
}

pub trait RangeExt {
    fn union(&self, other: &Self) -> Self;
}

impl<T: Ord + Copy> RangeExt for Range<T> {
    fn union(&self, other: &Self) -> Self {
        Range {
            start: self.start.min(other.start),
            end: self.end.max(other.end),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_range_union() {
        assert_eq!((1..5).union(&(2..6)), 1..6);
        assert_eq!((1..5).union(&(2..4)), 1..5);
        assert_eq!((2..6).union(&(1..5)), 1..6);
        assert_eq!((2..4).union(&(1..5)), 1..5);
        assert_eq!((3..5).union(&(10..12)), 3..12);
    }
}
