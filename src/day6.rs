use crate::scanner::*;
use aoc_runner_derive::{aoc, aoc_generator};
use std::{
    cell::RefCell,
    collections::{HashMap, HashSet},
    iter::successors,
    rc::Rc,
};

#[derive(Clone)]
struct Object {
    name: String,
    orbits: Option<Rc<RefCell<Object>>>,
    satellites: Vec<Rc<RefCell<Object>>>,
}

#[aoc_generator(day6)]
fn input_generator(input: &str) -> Vec<Rc<RefCell<Object>>> {
    let mut objects = HashMap::new();
    for line in input.lines() {
        let mut scanner = Scanner::new(line);
        let target_name = scanner.scan_up_to_str(")");
        let target = objects
            .entry(target_name)
            .or_insert_with(|| {
                Rc::new(RefCell::new(Object {
                    name: target_name.to_owned(),
                    orbits: None,
                    satellites: vec![],
                }))
            })
            .clone();
        scanner.scan_str(")");
        let satellite_name = scanner.remainder();
        let satellite = objects.entry(satellite_name).or_insert_with(|| {
            Rc::new(RefCell::new(Object {
                name: satellite_name.to_owned(),
                orbits: None,
                satellites: vec![],
            }))
        });
        target.borrow_mut().satellites.push(satellite.clone());
        satellite.borrow_mut().orbits = Some(target);
    }
    objects.into_iter().map(|(_k, v)| v).collect()
}

#[aoc(day6, part1)]
fn part1(input: &[Rc<RefCell<Object>>]) -> usize {
    input
        .into_iter()
        .map(|object| {
            successors(object.borrow().orbits.clone(), |obj| {
                obj.borrow().orbits.clone()
            })
            .count()
        })
        .sum()
}

#[aoc(day6, part2)]
fn part2(input: &[Rc<RefCell<Object>>]) -> usize {
    let you_start = input
        .into_iter()
        .find(|object| object.borrow().name == "YOU")
        .expect("Can't find YOU")
        .borrow()
        .orbits
        .clone()
        .expect("YOU aren't orbiting anything");
    let san_start = input
        .into_iter()
        .find(|object| object.borrow().name == "SAN")
        .expect("Can't find SAN")
        .borrow()
        .orbits
        .clone()
        .expect("SAN isn't orbiting anything");
    fn steps(
        start: &Rc<RefCell<Object>>, target: &str, seen: &mut HashSet<String>,
    ) -> Option<usize> {
        if start.borrow().name == target {
            return Some(0);
        }
        seen.insert(start.borrow().name.clone());
        start
            .borrow()
            .orbits
            .as_ref()
            .filter(|obj| !seen.contains(&obj.borrow().name))
            .as_ref()
            .and_then(|obj| steps(obj, target, seen))
            .or_else(|| {
                start
                    .borrow()
                    .satellites
                    .iter()
                    .filter(|obj| !seen.contains(&obj.borrow().name))
                    .collect::<Vec<_>>() // because of mutable reference to seen
                    .into_iter()
                    .find_map(|obj| steps(obj, target, seen))
            })
            .map(|x| x + 1)
    }
    let mut seen = HashSet::new();
    let target_name = &san_start.borrow().name;
    steps(&you_start, target_name, &mut seen).expect("Can't find path from YOU to SAN")
}
