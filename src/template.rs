#![allow(unused)]
use crate::{grid::*, helpers::*, intcode::*, scanner::*};
use anyhow::{anyhow, bail, ensure, Context, Result};
use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use lazy_static::lazy_static;
use rayon::prelude::*;
use regex::Regex;
use std::{
    collections::{HashMap, HashSet},
    iter::repeat,
    mem::replace,
    num::ParseIntError,
};

#[aoc_generator(day0)]
fn input_generator(input: &str) -> () {}

#[aoc(day0, part1)]
fn part1(input: &()) -> () {}

#[cfg(test)]
mod tests {
    use super::*;
}
