use crate::intcode::Intcode;
use anyhow::{Context, Result};
use aoc_runner_derive::{aoc, aoc_generator};
use std::num::ParseIntError;

#[aoc_generator(day2)]
fn input_generator(input: &str) -> Result<Vec<i32>, ParseIntError> {
    input.split(",").map(|s| s.parse()).collect()
}

#[aoc(day2, part1)]
fn part1(input: &[i32]) -> Result<i32> {
    let mut prog = Intcode::from(input);
    prog[1] = 12;
    prog[2] = 2;
    prog.run_program(vec![])?;
    Ok(prog[0])
}

#[aoc(day2, part2)]
fn part2(input: &[i32]) -> Result<i32> {
    let (noun, verb) = (0..=99)
        .flat_map(|a| (0..=99).map(move |b| (a, b)))
        .find(|&(noun, verb)| {
            let mut prog = Intcode::from(input);
            prog[1] = noun;
            prog[2] = verb;
            prog.run_program(vec![])
                .with_context(|| format!("noun={} verb={}", noun, verb))
                .expect("program failed to run");
            prog[0] == 19690720
        })
        .context("couldn't find expected output")?;
    Ok(100 * noun + verb)
}
